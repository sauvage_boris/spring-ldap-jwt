package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.AuthenticationToken;
import com.example.demo.util.AuthenticateCallback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by borissauvage on 20/06/2017.
 */
@Service
public class UserService {
    @Resource
    private UserRepository mUserRepository;
    @Autowired
    private LDAPService LDAPService;
    @Autowired
    private TokenService tokenService;

    public User authenticate(String username, String password) {
        Authentication authentication = LDAPService.authenticate(
                new UsernamePasswordAuthenticationToken(
                        username,
                        password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        User userInDatabase = mUserRepository.findByUsername(username);
        if (userInDatabase == null) {
            User userConnected = ((AuthenticationToken) authentication).getUser();
            return mUserRepository.save(userConnected);
        }


        return userInDatabase;
    }

    public User createUser(String firstName, String lastName) {
        User u = new User();
        u.setFirstName(firstName);
        u.setLastName(lastName);
        return this.mUserRepository.save(u);
    }

    public List<User> getUsers() {
        return (List<User>) this.mUserRepository.findAll();
    }

    public User loadUserByUsername(final String username) {
        return mUserRepository.findByUsername(username);
    }

    public void isAuthencated(HttpServletRequest request, String token, AuthenticateCallback<String> callback) {
        String username = tokenService.getUsernameFromToken(token);

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            User user = loadUserByUsername(username);

            if (tokenService.validateToken(username, token, user)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user
                        .getUsername(),
                        null,
                        user.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
                callback.onSuccess("");
            }
        }
        callback.onError("");
    }
}