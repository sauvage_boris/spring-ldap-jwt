package com.example.demo.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by borissauvage on 22/06/2017.
 */
@ConfigurationProperties(value = "application")
@Getter
@Setter
public class ApplicationConfig {
    private String version;
}
