package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Created by borissauvage on 28/06/2017.
 */
public class Error {

    private String error = null;
    private String message = null;
    private Object details = null;
    private LocalDate timestamp = null;

    /**
     **/
    public Error error(String error) {
        this.error = error;
        return this;
    }


    @JsonProperty("error")
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    /**
     **/
    public Error message(String message) {
        this.message = message;
        return this;
    }


    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     **/
    public Error details(Object details) {
        this.details = details;
        return this;
    }


    @JsonProperty("details")
    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    /**
     **/
    public Error timestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
        return this;
    }


    @JsonProperty("timestamp")
    public LocalDate getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Error error = (Error) o;
        return
                Objects.equals(error, error.error) &&
                        Objects.equals(message, error.message) &&
                        Objects.equals(details, error.details) &&
                        Objects.equals(timestamp, error.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(error, message, details, timestamp);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Error {\n");

        sb.append("    error: ").append(toIndentedString(error)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    details: ").append(toIndentedString(details)).append("\n");
        sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

