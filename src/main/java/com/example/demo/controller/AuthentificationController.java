package com.example.demo.controller;

import com.example.demo.UrlsPatterns;
import com.example.demo.builder.JsonResponseBuilder;
import com.example.demo.dto.AuthenticationRequestDTO;
import com.example.demo.dto.AuthenticationResponseDTO;
import com.example.demo.model.User;
import com.example.demo.service.TokenService;
import com.example.demo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by borissauvage on 20/06/2017.
 */
@RestController
public class AuthentificationController<T> {

    @Autowired
    JsonResponseBuilder mRes;

    @Autowired
    private UserService userService;

    @Value("${jwt.header}")
    private String tokenHeader;
    @Autowired
    private TokenService tokenService;


    @RequestMapping(value = UrlsPatterns.LOGIN, method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequestDTO authenticationRequest,
                                                       Device device) throws AuthenticationException {
        String username = authenticationRequest.getUsername();
        String password = authenticationRequest.getPassword();

        final User authenticatedUser = userService.authenticate(username, password);
        final String token = tokenService.generateToken(authenticatedUser, device);

        return mRes.content(new AuthenticationResponseDTO(token)).status(HttpStatus.OK).build();
    }

    //    @RequestMapping(value = UrlsPatterns.REFRESH, method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
//        String token = request.getHeader(tokenHeader);
//        String username = tokenService.getUsernameFromToken(token);
//        userService.loadUserByUsername(username);

//        if (tokenService.canTokenBeRefreshed(token)) {
//            String refreshedToken = tokenService.refreshToken(token);
//            return ResponseEntity.ok(new AuthenticationResponseDTO(refreshedToken));
//        } else {
//            return ResponseEntity.badRequest().body(null);
//        }

//    }
        //todo : implement this route
        return ResponseEntity.badRequest().body(null);
    }
}
