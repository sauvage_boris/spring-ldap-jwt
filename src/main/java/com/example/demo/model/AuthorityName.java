package com.example.demo.model;

/**
 * Created by borissauvage on 06/07/2017.
 */
public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}