package com.example.demo.controller;

import com.example.demo.UrlsPatterns;
import com.example.demo.builder.JsonResponseBuilder;
import com.example.demo.dto.UserListDTO;
import com.example.demo.model.User;
import com.example.demo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by borissauvage on 20/06/2017.
 */
@RestController
@RequestMapping(UrlsPatterns.USER)
public class UserController<T> {
    @Autowired
    JsonResponseBuilder mRes;

    @Autowired
    private UserService mUserService;

    @RequestMapping(value = "/lille", method = RequestMethod.GET, consumes = {"application/json"})
    public ResponseEntity<T> getLilleCollaborators() {
        List<User> users = mUserService.getUsers();
        return mRes.content(new UserListDTO(users)).build();
    }
}
