package com.example.demo;

/**
 * Created by borissauvage on 07/07/2017.
 */
public class UrlsPatterns {
    public final static String LOGIN = "/auth/login";
    public final static String REFRESH = "/auth/refresh";
    public static final String VERSION = "/version";
    public static final String USER = "/user";
}
