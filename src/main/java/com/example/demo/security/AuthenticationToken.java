package com.example.demo.security;

import com.example.demo.model.User;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by borissauvage on 06/07/2017.
 */
public class AuthenticationToken extends UsernamePasswordAuthenticationToken {

    private User user;

    public AuthenticationToken(final Object principal, final Object credentials) {
        super(principal, credentials);
    }

    public AuthenticationToken(final Object principal, final Object credentials, final Collection<? extends
            GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
}
