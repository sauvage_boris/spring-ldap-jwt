package com.example.demo.configuration;

import com.example.demo.model.User;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;

/**
 * Created by borissauvage on 07/07/2017.
 */
public class DatabaseInterceptor extends EmptyInterceptor {

    @Override
    public boolean onSave(final Object entity, final Serializable id, final Object[] state, final String[]
            propertyNames, final Type[] types) {
        if (entity instanceof User) {
            User user = ((User) entity);
            user.setUsername(user.getUsername().toLowerCase());
        }
        return false;
    }
}
