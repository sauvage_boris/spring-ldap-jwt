package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import lombok.Getter;

/**
 * Created by borissauvage on 27/06/2017.
 */
@Configuration
@Getter
public class LdapConfig {
    @Value("${ldap.url}")
    private String[] url;
    @Value("${ldap.base}")
    private String base;
    @Value("${ldap.userDn}")
    private String userDn;
    @Value("${ldap.password}")
    private String password;

    @Bean(name = "contextSource")
    @Scope("singleton")
    public ContextSource contextSource() {
        LdapContextSource ldapContextSource = new LdapContextSource();
        ldapContextSource.setUrls(this.url);
        ldapContextSource.setBase(this.base);
        ldapContextSource.setUserDn(this.userDn);
        ldapContextSource.setPassword(this.password);
        return ldapContextSource;
    }

    @Bean(name = "ldapTemplate")
    @Scope("singleton")
    public LdapTemplate ldapTemplate() {
        LdapTemplate ldapTemplate = new LdapTemplate(contextSource());
        return ldapTemplate;
    }
}