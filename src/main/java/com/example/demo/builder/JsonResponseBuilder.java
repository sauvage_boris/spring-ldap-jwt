package com.example.demo.builder;

/**
 * Created by borissauvage on 28/06/2017.
 */

import com.example.demo.model.Error;
import com.example.demo.model.JsonBodyFormInputError;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class JsonResponseBuilder facilitates the build of {@link ResponseEntity} object<br>
 * <p>
 * Examples of use : <br>
 * <p>
 * <pre>
 * getJsonResponseBuilder().status(HttpStatus.BAD_REQUEST).error(bindingResult)
 * .build();
 * getJsonResponseBuilder().status(HttpStatus.BAD_REQUEST)
 * .error(anAuchanException).build();
 * getJsonResponseBuilder().status(HttpStatus.OK).data(&quot;a message&quot;).build();
 * getJsonResponseBuilder().status(HttpStatus.OK).data(anObject).build();
 * getJsonResponseBuilder().status(HttpStatus.OK).data(aList).build();
 * </pre>
 */
@Component
public class JsonResponseBuilder {
    /**
     * Logger for this class
     */

    private final static String DEFAULT_TIMESTAMP_PATTERN = "dd/MM/yyyy HH:mm:ss:SSS";
    private final static String DEFAULT_ERROR_CODE = "UNKNOWN_ERROR";
    /**
     * FORM_VALIDATION_ERROR_CODE content in errors.code
     */
    public final static String FORM_VALIDATION_ERROR_CODE = "FORM_VALIDATION_ERROR";

    protected Object jsonBody;
    // Default status set to INTERNAL_SERVER_ERROR
    protected HttpStatus status;

    protected MultiValueMap<String, String> headers;

    /**
     * Sets an HTTP status code for JSON response
     *
     * @param status the status
     * @return the json response builder
     * @see {@link org.springframework.http.HttpStatus} values
     * @see <a href="http://www.iana.org/assignments/http-status-codes">HTTP Status Code Registry</a>
     * @see <a href="http://en.wikipedia.org/wiki/List_of_HTTP_status_codes">List of HTTP status codes - Wikipedia</a>
     */
    public JsonResponseBuilder status(HttpStatus status) {
        this.status = status;
        return this;
    }

    /**
     * Set HTTP headers depending on your needs.
     *
     * @param headers the headers
     * @return the json response builder
     */
    public JsonResponseBuilder headers(MultiValueMap<String, String> headers) {
        this.headers = headers;
        return this;
    }


    /**
     * Sets a content of {@link Object} type<br>
     * Content is generally one the following :<br>
     * <ul>
     * <li>{@link String}</li>
     * <li>{@link Object}</li>
     * <li>{@link java.util.Collection}, {@link List}, ...</li>
     * <li>an array of {@link Object}</li>
     * </ul>
     * <p>
     * Content must be set when response is {@link HttpStatus#is2xxSuccessful()}
     *
     * @param payload the data content
     * @return the current builder instance
     */
    public <U> JsonResponseBuilder content(U payload) {
        // setting content to JsonResponse is associated to HttpStatus.OK if not defined when calling content()
        if (this.status == null) {
            this.status = HttpStatus.OK;
        }
        this.jsonBody = payload;

        return this;
    }

    /**
     * Automatically built a response based on {@link BindingResult} object
     * <p>
     * <p>This method sets an error in response content resulting from the form validation
     * {@link org.springframework.validation.BindingResult}<br>
     * Conversion is done using {@link #convertBindingResultToErrorMap(BindingResult)} method</p>
     *
     * @param bindingResult the binding result to parse
     * @return
     */
    public JsonResponseBuilder error(BindingResult bindingResult) {
        Map<String, List<JsonBodyFormInputError>> errors = convertBindingResultToErrorMap(bindingResult);
        Error e = new Error();
        e.setError(FORM_VALIDATION_ERROR_CODE);
        if (bindingResult != null) {
            e.setMessage(String.format("%s error(s) founded while trying to validate form", bindingResult
                    .getErrorCount()));
        }
        e.setDetails(errors);

        this.jsonBody = e;
        this.status = HttpStatus.BAD_REQUEST;

        return this;
    }

    /**
     * Automatically built a {@link Error} based on {@link MethodArgumentNotValidException#getBindingResult()} object
     *
     * @param e Exception to be thrown when validation on an argument annotated with @Valid fails.
     * @return the current builder instance with {@link Error} filled
     * @see this{@link #error(BindingResult)} for details
     */
    public JsonResponseBuilder error(MethodArgumentNotValidException e) {
        return this.error(e.getBindingResult());
    }


    /**
     * Automatically built a responsebased on {@link Exception} object
     */
    public JsonResponseBuilder error(Exception e) {
        Error error = new Error();
        error.setError(DEFAULT_ERROR_CODE);
        error.setMessage(error.getMessage());
        this.jsonBody = error;
        return this;
    }


    /**
     * Call this method to finalize creation of the {@link ResponseEntity} object
     * <p>
     * <p> set {@link #status} to {@link HttpStatus#INTERNAL_SERVER_ERROR} in {@link #status} not defined thanks to
     * {@link #status(HttpStatus)} method</p>
     *
     * @return the {@link ResponseEntity}
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> ResponseEntity<T> build() {
        if (this.status == null) {
            this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity(this.jsonBody, this.headers, this.status);
        //jsonResponse = new JsonResponse(this.jsonBody, this.headers, this.status);
        //return jsonResponse;
    }

    /**
     * Convert binding result to an error map which will be put in the response
     * content
     *
     * @param bindingResult the binding result from form validation
     * @return the error map
     */
    private Map<String, List<JsonBodyFormInputError>> convertBindingResultToErrorMap(
            BindingResult bindingResult) {
        Map<String, List<JsonBodyFormInputError>> errors = null;
        if (bindingResult != null && bindingResult.hasErrors()) {
            errors = new HashMap<>();

            for (ObjectError error : bindingResult.getAllErrors()) {

                // build error
                JsonBodyFormInputError message = JsonBodyFormInputError.builder().build();
                if (error instanceof FieldError) {
                    message.setFieldId(((FieldError) error).getField());
                } else {
                    //global error messages
                    message.setFieldId(error.getObjectName());
                }
                message.setArguments(error.getArguments());
                message.setCode(error.getCodes()[0]);
                message.setMessage(message.getFieldId() + " " + error.getDefaultMessage());

                // add error
                if (errors.get(message.getFieldId()) != null) {
                    // Field is already in the list
                    errors.get(message.getFieldId()).add(message);
                } else {
                    // Field is not yet in the list
                    List<JsonBodyFormInputError> tempError = new ArrayList<JsonBodyFormInputError>();
                    tempError.add(message);
                    errors.put(message.getFieldId(), tempError);
                }
            }
        }
        return errors;
    }
}