package com.example.demo.model;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by borissauvage on 28/06/2017.
 */

@Builder
@Getter
@Setter
public class JsonBodyFormInputError {
    /**
     * use to linked message to form's field
     */
    private String fieldId;
    /**
     * Message to display.
     * Build thanks to {@link this#code} & {@link this#arguments} attributes
     */
    private String message;
    /**
     * i18n arguments' key
     */
    private Object[] arguments;
    /**
     * i18n code
     */
    private String code;
}
