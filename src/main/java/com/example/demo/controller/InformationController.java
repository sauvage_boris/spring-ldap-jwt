package com.example.demo.controller;

import com.example.demo.configuration.ApplicationConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.example.demo.UrlsPatterns.VERSION;

/**
 * Created by borissauvage on 27/06/2017.
 */

@RestController
@EnableConfigurationProperties(ApplicationConfig.class)
public class InformationController {

    @Autowired
    private ApplicationConfig mApplicationConfig;

    /**
     * @return
     */
    @RequestMapping(value = VERSION, method = RequestMethod.GET)
    public String getVersion() {
        return mApplicationConfig.getVersion();
    }
}

