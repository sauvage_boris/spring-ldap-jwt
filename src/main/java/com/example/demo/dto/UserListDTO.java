package com.example.demo.dto;

import com.example.demo.model.User;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by borissauvage on 03/07/2017.
 */
@Setter
@Getter
public class UserListDTO {

    public UserListDTO(final Collection<User> users) {
        this.users = users;
        this.size = users.size();
    }

    private Collection<User> users;
    private int size;
}
