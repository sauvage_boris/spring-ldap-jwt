package com.example.demo.security.filter;

import com.example.demo.service.UserService;
import com.example.demo.util.AuthenticateCallback;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AuthenticationTokenFilter extends OncePerRequestFilter {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_KEY_BEARER = "Bearer ";

    private final Log logger = LogFactory.getLog(this.getClass());
    private final UserService userService;

    public AuthenticationTokenFilter(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        // Intercept Bearer
        String authToken = request.getHeader(AUTHORIZATION_HEADER);
        if (authToken != null) {
            authToken.startsWith(TOKEN_KEY_BEARER);
            authToken = authToken.substring(7);

            userService.isAuthencated(request, authToken, new AuthenticateCallback<String>() {
                @Override
                public void onSuccess(final String value) {
                    logger.info("authenticated user " + value + ", setting security context");
                }

                @Override
                public void onError(final String value) {
                    logger.info("no authenticated " + value);
                }
            });
        }
        chain.doFilter(request, response);
    }
}