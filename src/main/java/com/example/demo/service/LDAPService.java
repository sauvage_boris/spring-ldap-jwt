package com.example.demo.service;

import com.example.demo.model.InternalServerError;
import com.example.demo.model.User;
import com.example.demo.security.AuthenticationToken;

import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.ldap.AuthenticationException;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapEntryIdentification;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.ldap.core.support.LookupAttemptingCallback;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.naming.directory.DirContext;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

/**
 * Created by borissauvage on 04/07/2017.
 */
@Service
public class LDAPService implements AuthenticationManager {

    private static final String BASE_LDAP_LILLE = "OU=INEAT,OU=NPC";
    private static final String ATTRIBUTE_SA_M_ACCOUNT_NAME = "saMAccountName";
    private static final String ATTRIBUTE_FIRSTNAME = "givenName";
    private static final String ATTRIBUTE_LASTNAME = "sn";
    private static final String ATTRIBUTE_MAIL = "mail";

    @Autowired
    private LdapTemplate mLdapTemplate;

    /**
     * Authenticate user UID from INEAT LDAP
     *
     * @param username
     * @param password
     */
    private User authenticate(String username, String password) {
        LdapQuery query = query().where(ATTRIBUTE_SA_M_ACCOUNT_NAME).is(username);

        try {
            DirContextOperations dirContextOperations = mLdapTemplate.authenticate(query, password, new
                    LookupAttemptingCallback() {
                        @Override
                        public DirContextOperations mapWithContext(final DirContext ctx, final
                        LdapEntryIdentification ldapEntryIdentification) {
                            return super.mapWithContext(ctx, ldapEntryIdentification);
                        }
                    });
            return createUserFrom(dirContextOperations);

        } catch (EmptyResultDataAccessException wrongUUID) {
            return null;
        } catch (AuthenticationException wrongPassword) {
            return null;
        } catch (Exception ex) {throw new InternalServerError(ex);
        }
    }

    /**
     * Fetch all users in Active Directory INEAT Lille
     *
     * @return List of users objects
     */
    public List<User> getUsersFromLille() {
        LdapQueryBuilder builder = query();
        builder.base(BASE_LDAP_LILLE);
        builder.where("objectClass").is("user");

        return mLdapTemplate.search(builder, new
                AbstractContextMapper<User>() {
                    @Override
                    protected User doMapFromContext(final DirContextOperations dirContextOperations) {
                        return createUserFrom(dirContextOperations);
                    }
                });
    }

    private User createUserFrom(DirContextOperations dirContextOperations) {
        String ldapLogin = dirContextOperations.getStringAttribute(ATTRIBUTE_SA_M_ACCOUNT_NAME);
        String firstName = dirContextOperations.getStringAttribute(ATTRIBUTE_FIRSTNAME);
        String lastName = dirContextOperations.getStringAttribute(ATTRIBUTE_LASTNAME);
        String email = dirContextOperations.getStringAttribute(ATTRIBUTE_MAIL);

        return new User(ldapLogin, firstName, lastName, email);
    }

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrincipal() + "";
        String password = authentication.getCredentials() + "";

        User user = this.authenticate(username, password);
        if (user == null) {
            throw new BadCredentialsException("1000");
        }
        AuthenticationToken auth = new AuthenticationToken(
                authentication.getPrincipal(),
                authentication.getCredentials(),
                authentication.getAuthorities());
        auth.setUser(user);
        return auth;
    }
}
