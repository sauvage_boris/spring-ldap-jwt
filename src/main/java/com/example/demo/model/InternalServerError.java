package com.example.demo.model;


import org.springframework.ldap.CommunicationException;

/**
 * Created by borissauvage on 11/07/2017.
 */
public class InternalServerError extends RuntimeException {

    private ExceptionCodes exceptionCodesCode;

    public InternalServerError(Exception ex) {
        if (ex instanceof CommunicationException) {
            this.exceptionCodesCode = ExceptionCodes.TIME_OUT_LDAP;
        } else {
            this.exceptionCodesCode = ExceptionCodes.UNKNOW_ERROR;
        }
    }

    public InternalServerError(ExceptionCodes codes) {
        this.exceptionCodesCode = codes;
    }

    @Override
    public String getMessage() {
        return exceptionCodesCode.getCode();
    }

    public enum ExceptionCodes {
        TIME_OUT_LDAP("Operation time out ldap"),
        UNKNOW_ERROR("Unknow error");

        private String code;

        private ExceptionCodes(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
}