package com.example.demo.dto;

import java.io.Serializable;

/**
 * Created by borissauvage on 06/07/2017.
 */
public class AuthenticationResponseDTO implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;

    public AuthenticationResponseDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}
