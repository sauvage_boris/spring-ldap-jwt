package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by borissauvage on 28/06/2017.
 */
@Getter
@Setter
public class LoginDTO {
    private String username;
    private String password;
}
