package com.example.demo.util;

/**
 * Created by borissauvage on 07/07/2017.
 */
public interface AuthenticateCallback<T> {

    void onSuccess(T value);

    void onError(T value);
}
